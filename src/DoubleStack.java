import java.util.*;

public class DoubleStack 
	{
	private LinkedList<Double> list;
	
	public static void main (String[] argum) 
		{
		DoubleStack m = new DoubleStack();
		System.out.println(m);
		m.push(1);
		System.out.println(m);
		m.push(3);
		System.out.println(m);
		m.push(6);
		System.out.println(m);
		m.push(2);
		System.out.println(m);
		m.op("/");
		System.out.println(m);
		m.op("*");
		System.out.println(m);
		m.op("-");
		System.out.println(m);
		double result = m.pop();
		System.out.println(m);
		System.out.println(result);
		DoubleStack copy = m;
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		try 
			{
			copy = (DoubleStack) m.clone();
			} 
		catch (CloneNotSupportedException e) 
			{
			}
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		m.push(6);
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		m.pop();
		System.out.println(copy.equals(m));
		System.out.println(m);
		System.out.println(copy);
		String prog = "2 3 + 4 * 10 /";
		if (argum.length > 0) 
			{
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < argum.length; i++) 
				{
				sb.append(argum[i]);
				sb.append(" ");
				}
			prog = sb.toString();
			}
		
		System.out.println(prog + "\n " 
                  + String.valueOf(interpret(prog)));

		}

	
	DoubleStack() 
   		{
		list = new LinkedList<Double>();
   		}

	
	@Override
	public Object clone() throws CloneNotSupportedException 
   		{
	   	DoubleStack tmp = new DoubleStack();
	   	LinkedList<Double> clone = (LinkedList<Double>) list.clone();
	   	tmp.list = clone;
	   	return tmp;
   		}


   public boolean stEmpty() 
   		{
	   	return list.isEmpty();  //vb l�bi this.list?
   		}

   
   public void push (double a) 
   		{
	   	list.push(a);
   		}

   
   public double pop() 
   		{
	   	if (stEmpty())
	   		throw new IndexOutOfBoundsException("Stack Underflow");
	   	return list.pop();
   		}
   
   
   public void op (String s) 
   		{
	   	if (stEmpty())
	   		throw new IndexOutOfBoundsException("Stack Underflow");
	   	else if (list.size() < 2)
	   		throw new RuntimeException("Too few elements to work with"); 
	   	char op = s.charAt(0);
		double el1 = list.remove(1);
		double el2 = list.remove(0);
		switch (op) 
			{
			case '+':
				push(el1 + el2);
				break;
			case '-':
				push(el1 - el2);
				break;
			case '*':
				push(el1 * el2);
				break;
			case '/':
				push(el1 / el2);
				break;
			default:
				throw new RuntimeException("Illegal operation '" + op + "' (valid operations are +, -, * and /).");
			}
   		}

   
   public double tos() 
   		{
	   	if (stEmpty())
	   		throw new IndexOutOfBoundsException("Stack Underflow");		
	   
	   	return list.peek();
   		}

   
   @Override
   public boolean equals (Object o) 
   		{
	   	DoubleStack list2 = new DoubleStack();
	   	if (list.equals(((DoubleStack) o).list))
	   		return true;
	   	else 
	   		return false;
   		}

   
   @Override
   public String toString() 
   		{
		StringBuffer ret = new StringBuffer();
		for (int i = list.size() - 1; i >= 0; i--) 
			{
			ret.append(list.get(i));
			if (i > 0)
				ret.append(' ');
			}
		return ret.toString();
   		}

   
   public static double interpret (String pol) 
   		{
	   	if (pol == null || pol.length() == 0)
	   		throw new RuntimeException("Expression can't be empty");
	   	DoubleStack list = new DoubleStack();
	   	StringTokenizer cut = new StringTokenizer(pol, " \t");	
	   	int i = 1;
	   	int counter = cut.countTokens();
	   	while (cut.hasMoreTokens()) 
	   		{
	   		String part = (String) cut.nextElement();
	   		if (part.equals("-") || part.equals("+") || part.equals("/") || part.equals("*")) 
	   			list.op(part);
	   		else
	   			{
	   			if (counter == i && i > 2) 
	   				throw new RuntimeException("Expression '" + pol + "' has " + counter + " numbers but only " + i + " operants" );
	   			try
	   				{
	   				double a = Double.parseDouble(part);
	   				list.push(a);
	   				}
	   			catch(NumberFormatException e) 
	   				{  
	   				System.out.println("Expression:  " + pol + "     where " + part + " is not valid"); 
	   				}  
	   			}
	   		i++;
	   		}

	   	return list.tos();
		} 
   }



/* VIITED:
 * http://enos.itcollege.ee/~jpoial/algorithms/examples/Astack.java
 * https://git.wut.ee/i231/home3/src/master/src/LongStack.java
 * http://enos.itcollege.ee/~ylari/I231/Intstack.java
 * http://stackoverflow.com/questions/26532871/how-to-clone-a-double-linked-list-using-recursivity
 * 
 * kaas�pilaste abi
 */

